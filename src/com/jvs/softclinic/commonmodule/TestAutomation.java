package com.jvs.softclinic.commonmodule;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;
import com.jvs.softclinic.model.EntityMapping;
import com.jvs.softclinic.model.GenericResponse;
import com.jvs.softclinic.model.TestCase;

import java.util.ArrayList;
import java.util.HashMap;

import static com.jayway.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

public class TestAutomation {
	static JsonParser parser = new JsonParser();
	public static int countPass = 0;
	public 	static int countFail = 0;
	public  static  int ActualCode=0;
	public  static  int ExpectedCode=0;
	public static String url;
	private static final String TESTRUNEXECTIME_EXP = "\\$\\{TESTRUNEXECTIME}";
	// private static final String TESTRUNEXECTIME_VALUE =  String.valueOf((int) (Math.random() * 100000));

	int totalPass=0;
	int totalFail=0;


//	public static int finalcountpost;

	public static void get(TestCase testCase) {
		System.out.println("---------------------------------------------------------------------------------");
		//	try {
		System.out.println("Get Request : \n" + testCase);

		String entityName = testCase.getMeta().getEntity().toLowerCase();
		// String url = EntityMapping.getMappings().get(entityName).get("url");
		if(EntityMapping.getMappings().get(entityName)==null) return;
		//	Entity entity = EntityMapping.getMappings().get(entityName);
		String id = testCase.getData().get("id").getAsString();
		JsonObject expectedResponse = testCase.getResponse().getAsJsonObject("responseObject");

		System.out.print("Expected : " + expectedResponse);

		Response actualResponse = given()
				.log().all().header(new Header("Authorization", "Bearer " + RunTestCase.getAccessToken()))
				.header(new Header("Content-Type", "application/json"))
				.when().get(url + id);

		GenericResponse receivedResponse = new GenericResponse(actualResponse.getStatusCode(), actualResponse.getBody().asString());

		if (receivedResponse.getStatusCode() == Integer.parseInt(String.valueOf(testCase.getResponse().get("statusCode")))) {
			countPass++;

			System.out.println("Expected Response code: " + testCase.getResponse().get("statusCode") +
					" Got: " + receivedResponse.getStatusCode());

			System.out.println("Get response : Status Matched");
			System.out.println("Testcase is : Pass");

		} else {

			countFail++;
			System.out.println("Expected Response code: " + testCase.getResponse().get("statusCode") +
					" Got: " + receivedResponse.getStatusCode());

			System.out.println("Get response : Status not matched");
			System.out.println("Testcase is : Fail");


		}
		System.out.println("Pass Get Results =: " + countPass);
		System.out.println("Fail Get Results =: " + countFail);
		ActualCode=receivedResponse.getStatusCode();
		ExpectedCode=Integer.parseInt(String.valueOf(testCase.getResponse().get("statusCode")));

		//	finalcountpost = countPass + countFail;

		assertEquals(parser.parse(receivedResponse.getResponseObject()), parser.parse(expectedResponse.toString()));

	}

	public static void post(TestCase testCase) {
		System.out.println("---------------------------------------------------------------------------------");
		String TESTRUNEXECTIME_VALUE =  String.valueOf((int) (Math.random() * 100000));
		//try {
		System.out.println("Post case : \n" + testCase);
		String entityName = testCase.getMeta().getEntity().toLowerCase();
		//Entity entity = EntityMapping.getMappings().get(entityName).get("url");
		if(EntityMapping.getMappings().get(entityName) == null) return;
		HashMap<String, Object> entityMap = (HashMap<String, Object>)EntityMapping.getMappings().get(entityName);
		String url = (String)entityMap.get("url");
		ArrayList<String> suffixList = (ArrayList<String>) entityMap.get("SuffixRandomString");
		JsonObject reqBody = testCase.getData();

		if(suffixList!=null) {
			for (String elementName : suffixList) {
				if (reqBody.has(elementName)) {
					JsonElement jsonElement = reqBody.get(elementName);
					String elementValue = jsonElement.getAsString();
					elementValue = elementValue.replaceAll(TESTRUNEXECTIME_EXP,TESTRUNEXECTIME_VALUE);
					reqBody.addProperty(elementName, elementValue);
					System.out.println("Adding random value for element: " + elementName + " value: " + elementValue);
				}
			}
		}

		JsonObject expectedResponse = testCase.getResponse().getAsJsonObject("responseObject");
		System.out.println("Expected response : " + expectedResponse);
		Response actualResponse = given()
				.log().all().header(new Header("Authorization", "Bearer " + RunTestCase.getAccessToken()))
				.contentType("application/json")
				.body(reqBody)
				.when().post(url);

		GenericResponse receivedResponse = new GenericResponse(actualResponse.getStatusCode(), actualResponse.getBody().asString());
		if (receivedResponse.getStatusCode() == Integer.parseInt(testCase.getResponse().get("statusCode").toString())) {
			countPass++;

			System.out.println("Expected Response code: " + testCase.getResponse().get("statusCode") +
					" Got: " + receivedResponse.getStatusCode());

			System.out.println("Post response : Status Matched");

			System.out.println("Testcase is : Pass");
		} else {
			countFail++;

			System.out.println("Expected Response code: " + testCase.getResponse().get("statusCode") +
					" Got: " + receivedResponse.getStatusCode());

			System.out.println("Post response : Status not matched");
			System.out.println("Testcase is : Fail");


		}
		System.out.println("Pass Get Results =: " + countPass);
		System.out.println("Fail Get Results =: " + countFail);
		ActualCode=receivedResponse.getStatusCode();
		ExpectedCode=Integer.parseInt(String.valueOf(testCase.getResponse().get("statusCode")));

		//finalcountpost = countPass + countFail;
		assertEquals(parser.parse(receivedResponse.getResponseObject()), parser.parse(expectedResponse.toString()));
		//return finalcountpost;
	}

	public static void put(TestCase testCase) {
		System.out.println("---------------------------------------------------------------------------------");
		//try {
		System.out.println("Put case : \n" + testCase);
		String entityName = testCase.getMeta().getEntity().toLowerCase();
		if(EntityMapping.getMappings().get(entityName)==null) return;
		HashMap<String, Object> entityMap = (HashMap<String, Object>)EntityMapping.getMappings().get(entityName);
		String url = (String)entityMap.get("url");
		String id = testCase.getData().get("id").getAsString();
		JsonObject reqBody = testCase.getData();
		JsonObject expectedResponse = testCase.getResponse().getAsJsonObject("responseObject");

		Response actualResponse = given()
				.log().all().header(new Header("Authorization", "Bearer " + RunTestCase.getAccessToken()))
				.contentType("application/json")
				.body(reqBody)
				.when().put(url + id);

		GenericResponse receivedResponse = new GenericResponse(actualResponse.getStatusCode(), actualResponse.getBody().asString());

		if (receivedResponse.getStatusCode() == Integer.parseInt(String.valueOf(testCase.getResponse().get("statusCode")))) {
			countPass++;

			System.out.println("Expected Response code: " + testCase.getResponse().get("statusCode") +
					" Got: " + receivedResponse.getStatusCode());

			System.out.println("Put response : Status Matched");

			System.out.println("Testcase is : Pass");

		} else {

			countFail++;

			System.out.println("Expected Response code: " + testCase.getResponse().get("statusCode") +
					" Got: " + receivedResponse.getStatusCode());

			System.out.println("Put response : Status not matched");
			System.out.println("Testcase is : Fail");


		}
		System.out.println("Pass Get Results =: " + countPass);
		System.out.println("Fail Get Results =: " + countFail);
		ActualCode=receivedResponse.getStatusCode();
		ExpectedCode=Integer.parseInt(String.valueOf(testCase.getResponse().get("statusCode")));

		//finalcountpost = countPass + countFail;
		try {
			// String actual = receivedResponse.getResponseObject();
			// assertEquals(parser.parse(receivedResponse.getResponseObject()), parser.parse(expectedResponse.toString()));
			assertEquals(receivedResponse.getResponseObject(), expectedResponse.toString());

		}catch (Exception error){
			error.printStackTrace();
		}
	}

	public static void delete(TestCase testCase) {
		System.out.println("---------------------------------------------------------------------------------");
		//try {
		System.out.println("Delete Request : \n" + testCase);
		String entityName = testCase.getMeta().getEntity().toLowerCase();
		if(EntityMapping.getMappings().get(entityName)==null) return;
		//	Entity entity = EntityMapping.getMappings().get(entityName);
		String id = testCase.getData().get("id").getAsString();
		JsonObject expectedResponse = testCase.getResponse().getAsJsonObject("responseObject");

		Response actualResponse = given()
				.log().all().header(new Header("Authorization", "Bearer " + RunTestCase.getAccessToken()))
				.header(new Header("Content-Type", "application/json"))
				.when().delete(url + id);

		GenericResponse receivedResponse = new GenericResponse(actualResponse.getStatusCode(),
				actualResponse.getBody().asString());

		if (receivedResponse.getStatusCode() == Integer.parseInt(String.valueOf(testCase.getResponse().get("statusCode")))) {

			countPass++;

			System.out.println("Expected Response code: " + testCase.getResponse().get("statusCode") +
					" Got: " + receivedResponse.getStatusCode());

			System.out.println("Delete response : Status Matched");

			System.out.println("Testcase is : Pass");


		} else {

			countFail++;

			System.out.println("Expected Response code: " + testCase.getResponse().get("statusCode") +
					" Got: " + receivedResponse.getStatusCode());

			System.out.println("Delete response : Status not matched");
			System.out.println("Testcase is : Fail");

		}

		System.out.println("Pass Get Results =: " + countPass);
		System.out.println("Fail Get Results =: " + countFail);
		ActualCode=receivedResponse.getStatusCode();
		ExpectedCode=Integer.parseInt(String.valueOf(testCase.getResponse().get("statusCode")));


		//finalcountpost = countPass + countFail;
		assertEquals(parser.parse(receivedResponse.getResponseObject()), parser.parse(expectedResponse.toString()));


	}


}














