package com.jvs.softclinic.commonmodule;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.path.json.JsonPath;
import com.jvs.softclinic.FunctionalTest;
import com.jvs.softclinic.model.*;
import com.jvs.softclinic.scheduler.RefreshTokenScheduler;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.NameValuePair;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

import static com.jayway.restassured.RestAssured.rootPath;
//import org.junit.jupiter.api.Test;


public class RunTestCase extends FunctionalTest {
	int count = 0;


	private static String ACCESS_TOKEN;
	private static String REFRESH_TOKEN;

	private static final String RESOURCES = "resources/";
	private static final String ENTITY_MAPPING = "resources/entitymapping.json";
	private static final String TEST_SCENARIOS = RESOURCES+"/testscenarios";

	//	@BeforeClass
//	public static void getEntityMapping(){
	static{
		File jsonExample = new File(System.getProperty("user.dir"),ENTITY_MAPPING);
		Map<String,Object> mapping = new JsonPath(jsonExample).getJsonObject(rootPath);
		EntityMapping.setMappings(mapping);
	}

	//private String NewModuleName;

	public static String getAccessToken() {
		return ACCESS_TOKEN;
	}

	public static void setAccessToken(String accessToken) {
		ACCESS_TOKEN = accessToken;
	}

	public static String getRefreshToken() {
		return REFRESH_TOKEN;
	}

	public static void setRefreshToken(String refreshToken) {
		REFRESH_TOKEN = refreshToken;
	}

	@Test
	public void testScenarios(){
		File[] testScenarioDir = new File(System.getProperty("user.dir"), TEST_SCENARIOS).listFiles();
		Set<File> scenariosFileSet = new TreeSet<>();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		SimpleDateFormat dirformat = new SimpleDateFormat("dd-MM-yyyy");

		Date date = new Date();
		String DirectoryPath="C:\\APIAutomationSummaryReport";
		String foldername=DirectoryPath+"\\"+dirformat.format(date);
		File FileDir = new File(foldername);
		try{
			FileDir.mkdir();

		}catch (SecurityException e){

		}

		//Prepare Test Scenario List

		for (int i = 0; i < testScenarioDir.length; i++) {
			File directory = testScenarioDir[i];
			if (directory.isDirectory()) {
				File[] files = directory.listFiles();
				for (int j = 0; j < files.length; j++) {
					TestScenario testScenario = new JsonPath(files[j]).getObject(rootPath, TestScenario.class);
					processScenario(testScenario, scenariosFileSet);
				}
			}
		}

		System.out.println("\n\nFinal File List: "+scenariosFileSet);
		StopWatch watch = new StopWatch();


		//Execute Test Scenario List
		Iterator<File> files = scenariosFileSet.iterator();

		//String URL = new RestAssured().DEFAULT_URI;
		//String Username = new RefreshTokenScheduler().getRefreshToken().Username;
		String URL = RestAssured.baseURI;
		List<NameValuePair> form = new ArrayList<>();
		String Username = new RefreshTokenScheduler().UserName;
		String Version = new RefreshTokenScheduler().verSion;
		try{
			//java.util.Date date= new java.util.Date();
			//Timestamp  a=new Timestamp(date.getTime());

			//String temp=a.toString();
			//System.out.println(temp);
			//File f1=new File(temp);
			//System.out.println(f1.mkdir());

			String Checkfilenamepath=FileDir.getPath();
			FileWriter fw2=new FileWriter(Checkfilenamepath+"\\Testcase Wise Summary Report.htm");

			fw2.write("<h3>Testcase Wise Summary Report</h3>");
			fw2.write("<html><head>\n" +
					"  <title>Bootstrap Example</title>\n" +
					"  <meta charset=\"utf-8\">\n" +
					"  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
					"  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\">\n" +
					"  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>\n" +
					"  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\"></script>\n" +
					"</head><body>");
			fw2.write("<table border=\"2\"  class=\"table\"><tr style=\"background-color:#D3D3D3\"><th>Sr.No</th><th>File Name</th><th>Pass</th><th>Fail</th><th>Module Name</th></tr>");
			while(files.hasNext()){

				File file = files.next();

				++count;
				//watch.start();
				// call to the methods you want to benchmark



				System.out.println("\n\nExecuting Test Case: "+file+"\n");
				System.out.println("\n\nFile Name: "+file.getName()+"\n");

				System.out.println("\n\nFile Name: "+file.getParent().substring(file.getParent().lastIndexOf('\\')+1)+"\n");

				String fName= file.getName().toString();
				String mName=file.getParent().substring(file.getParent().lastIndexOf('\\')+1);

				int  oldtotalPass= new TestAutomation().countPass;
				int oldtotalFail = new TestAutomation().countFail;
				processFile(file);
				int  totalPass= new TestAutomation().countPass;
				int totalFail = new TestAutomation().countFail;
				if(oldtotalPass<totalPass){

					totalPass=1;
					totalFail=0;

				}
				else {
					totalPass=0;
					totalFail=1;
					int Length=fName.length()-5;

					String NewFileName=fName.substring(0,Length);

					int Usindex=fName.indexOf("_");
					String Id=NewFileName;
					//String Md=NewModuleName;
					//String Module=NewModuleName;
					if(Usindex==-1){

					}else{
						Id=fName.substring(0,Usindex);
						//Md=mName.toString();
					}

					System.out.println(formatter.format(date));
					System.out.println(LocalDateTime.now());
					FileWriter fw3=new FileWriter(FileDir.getPath()+"\\"+NewFileName+".html");
					//FileWriter fw3=new FileWriter(FileDir.getPath()+"\\"+NewModuleName+"");
					fw3.write(" <h3>Fail Testcase Summary Report</h3>");

					fw3.write("<html><head>\n" +
							"  <title>Bootstrap Example</title>\n" +
							"  <meta charset=\"utf-8\">\n" +
							"  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
							"  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\">\n" +
							"  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>\n" +
							"  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\"></script>\n" +

							"</head><body>");

					fw3.write("<table border=\"2\"  class=\"table\"><tr style=\"background-color:#D3D3D3\" ><th>TestCase_ID</th><th>TestCase_Name</th><th>Date</th></tr>" +
							"<tr><td>"+Id+"</td><td>"+NewFileName+"</td><td>"+formatter.format(date)+"</td></tr>"+
							"<tr><th colspan='2'>Expected Result:</th><td>"+ new TestAutomation().ExpectedCode+"</td></tr>"+
							"<tr><th colspan='2'>Actual Result:</th><td>"+new TestAutomation().ActualCode+"</td></tr>"+
							"</table>");
					fw3.write("</body></html>");
					fw3.close();

				}

				String NewFileName=fName.substring(0,fName.length()-5);
				String Path=FileDir.getPath()+"\\"+NewFileName+".html";
				//String=FileDir.getParent()+"\\"+mName+"";
				String replaceString=Path.replace('\\','/');
				fw2.write("<tr><td>"+count+"</td><td><a  href="+replaceString+" target=\"_blank\">"+fName+"</td><td>"+totalPass+"</td><td>"+totalFail+"</td><td>"+mName+"</td></tr>");


			}
			fw2.write("<tr><td>"+"Grand Total:"+"</td><td>"+""+"</td><td>"+new TestAutomation().countPass+"</td><td>"+new TestAutomation().countFail+"</td></tr>");
			fw2.write("</table></body></html>");
			fw2.close();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println(e);
		}
		System.out.println("Success File...");

		System.out.println("Total Number of TestCases Executed : " + count) ;
		//watch.stop();
		//long result = watch.getTime();
		//System.out.println("Total Execution Time : " + result+" ");



		try{
			int  totalPass= new TestAutomation().countPass;
			int totalFail = new TestAutomation().countFail;

			FileWriter fw=new FileWriter(FileDir.getPath()+"\\Test Summary Report.htm");
			fw.write("<html><head>\n" +
					"  <title>Bootstrap Example</title>\n" +

					"  <meta charset=\"utf-8\">\n" +
					"  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
					"  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\">\n" +
					"  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>\n" +
					"  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\"></script>\n" +

					"</head><body>");
			fw.write("<h3>Test Summary Report</h3>");
			fw.write("<table border=\"2\"  class=\"table\"><tr style=\"background-color:#D3D3D3 \"><th>Pass </th><th>Fail </th><th>Total </th><th>Date & Time </th><td>URL</td><td>Username</td><td>Version</td></tr><tr><td>"+totalPass+"</td><td>"+totalFail+"</td><td>"+count+"</td><td>"+formatter.format(date)+"</td><td>"+RestAssured.baseURI+"</td><td>"+Username+"</td><td>"+Version+"</td></tr></table>");
			fw.write("</body></html>");
			fw.close();
		}catch(Exception e){System.out.println(e);}
		System.out.println("Success...");






	}

	private void processScenario(TestScenario testScenario, Set<File> scenariosFileSet){

		for(int i=0;i<testScenario.getTestCases().length;i++) {
			TestScenarioDetails testCase = testScenario.getTestCases()[i];

			switch (testCase.getType().toUpperCase()) {
				case "TESTCASE":
					System.out.println("Adding TestCase: " + testCase.getFile());
					scenariosFileSet.add(new File(testCase.getFile()));
					break;
				case "TESTSCENARIO":
					System.out.println("Adding TestScenario: " + testCase.getFile());
					TestScenario tmpScenario = new JsonPath(new File(System.getProperty("user.dir"), testCase.getFile())).getObject(rootPath, TestScenario.class);
					processScenario(tmpScenario, scenariosFileSet);
					break;
			}

		}
	}


	private void processFile(File file){
		//TODO Different Wrapper Class for each testing
		TestCase wrapper = new JsonPath(file).getObject(rootPath, TestCase.class);
		System.out.println("Processing file: "+file.getName());
		try{
			switch(wrapper.getMeta().getOperation().toUpperCase()) {
				case "GET":
					TestAutomation.get(wrapper);
					break;
				case "POST":
					System.out.println("Token::" + getAccessToken());
					TestAutomation.post(wrapper);
					break;
				case "PUT":
					TestAutomation.put(wrapper);
					break;
				case "DELETE":
					TestAutomation.delete(wrapper);
					break;
			}
		}catch(Throwable exp){
			System.out.println("Error: "+wrapper.getMeta().getTestCaseId()+" "+exp.getMessage());
		}
	}



}
