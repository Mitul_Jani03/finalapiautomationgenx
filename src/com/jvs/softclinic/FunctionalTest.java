package com.jvs.softclinic;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jayway.restassured.RestAssured;
import com.jvs.softclinic.commonmodule.RunTestCase;
import com.jvs.softclinic.scheduler.AccessTokenScheduler;
import com.jvs.softclinic.scheduler.RefreshTokenScheduler;
import org.json.JSONException;
import org.junit.BeforeClass;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import java.util.Timer;
import java.util.TimerTask;

public class FunctionalTest {

    @BeforeClass
    public static void setup() {
        String port = System.getProperty("server.port");
        if (port == null) {
            RestAssured.port = Integer.valueOf(8080);
        }
        else{
            RestAssured.port = Integer.valueOf(port); 
        }

        String basePath = System.getProperty("server.base");
        if(basePath==null){
            basePath = "";
        }
        RestAssured.basePath = basePath;

        String baseHost = System.getProperty("server.host");
        if(baseHost==null){
          baseHost = "http://103.215.159.254:9090";
            
      
            
        	//baseHost="http://192.168.0.175:9090";
        }
        RestAssured.baseURI = baseHost;
        System.out.println("RestAssured.baseURI:"+RestAssured.baseURI);

        TimerTask accessTokenTimer = new AccessTokenScheduler();
        TimerTask refreshTokenTimer = new RefreshTokenScheduler();

        //running timer task as daemon thread
        Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(accessTokenTimer, 3600*1000, 3600*1000);
        timer.scheduleAtFixedRate(refreshTokenTimer,0 , 86400*1000);

    }

    public static void jsonCompare() throws JSONException {
        JsonParser parser = new JsonParser();

        Gson g = new Gson();
        JsonObject json1 = new JsonObject();
        JsonObject json2 = new JsonObject();
        JsonObject subJson = new JsonObject();
        JsonObject subJson1 = new JsonObject();

        subJson.addProperty("sub1","1");
        subJson.addProperty("sub2","2");
        subJson.addProperty("sub3","3");

        subJson1.addProperty("sub1","1");
        subJson1.addProperty("sub2","2");
        subJson1.addProperty("sub3","3");

        json1.addProperty("id","1");
        json1.addProperty("name","ABCD");
        json1.addProperty("type","1");
        json1.addProperty("innerjson", String.valueOf(subJson));


        json2.addProperty("name","ABCD");
        json2.addProperty("type","1");
        json2.addProperty("innerjson", String.valueOf(subJson1));

        JSONAssert.assertEquals(json2.toString(),json1.toString(), JSONCompareMode.LENIENT);



        /*Type mapType = new TypeToken<Map<String, Object>>(){}.getType();
        Map<String, Object> firstMap = g.fromJson(json1, mapType);
        Map<String, Object> secondMap = g.fromJson(json2, mapType);
        System.out.println(Maps.difference(firstMap, secondMap));

        MapDifference md = Maps.difference(firstMap, secondMap);
        System.out.println("equal::"+md.areEqual());
        System.out.println("Left::"+md.entriesOnlyOnLeft());
        System.out.println("Right::"+md.entriesOnlyOnRight());
*/

      /*  Iterator itr = json2.entrySet().iterator();
        System.out.println("size::"+json2.entrySet().size());
        while(itr.hasNext()){
            Object obj = itr.next();
            System.out.println("Obj::"+obj + obj.getClass());
        }*/
    }

    /*public static boolean compareJson(JsonElement json1, JsonElement json2) {
        boolean isEqual = true;
        // Check whether both jsonElement are not null
        if(json1 !=null && json2 !=null) {

            // Check whether both jsonElement are objects
            if (json1.isJsonObject() && json2.isJsonObject()) {
                Set<Map.Entry<String, JsonElement>> ens1 = ((JsonObject) json1).entrySet();
                Set<Map.Entry<String, JsonElement>> ens2 = ((JsonObject) json2).entrySet();
                JsonObject json2obj = (JsonObject) json2;
                if (ens1 != null && ens2 != null && (ens2.size() == ens1.size())) {
                    // Iterate JSON Elements with Key values
                    for (Map.Entry<String, JsonElement> en : ens1) {
                        isEqual = isEqual && compareJson(en.getValue() , json2obj.get(en.getKey()));
                    }
                } else {
                    return false;
                }
            }

            // Check whether both jsonElement are arrays
            else if (json1.isJsonArray() && json2.isJsonArray()) {
                JsonArray jarr1 = json1.getAsJsonArray();
                JsonArray jarr2 = json2.getAsJsonArray();
                if(jarr1.size() != jarr2.size()) {
                    return false;
                } else {
                    int i = 0;
                    // Iterate JSON Array to JSON Elements
                    for (JsonElement je : jarr1) {
                        isEqual = isEqual && compareJson(je , jarr2.get(i));
                        i++;
                    }
                }
            }

            // Check whether both jsonElement are null
            else if (json1.isJsonNull() && json2.isJsonNull()) {
                return true;
            }

            // Check whether both jsonElement are primitives
            else if (json1.isJsonPrimitive() && json2.isJsonPrimitive()) {
                if(json1.equals(json2)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else if(json1 == null && json2 == null) {
            return true;
        } else {
            return false;
        }
        return isEqual;
    }*/
    public static void main(String args[]){
        setup();

        try {
            System.out.println("Waiting for tokens availability");
            Thread.sleep(4000); //Put more wait if tokens are not available
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new RunTestCase().testScenarios();
    }

}
