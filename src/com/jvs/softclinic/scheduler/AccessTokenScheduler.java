package com.jvs.softclinic.scheduler;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jayway.restassured.RestAssured;
import com.jvs.softclinic.commonmodule.RunTestCase;
import com.jvs.softclinic.constants.APIConstants;
import com.jvs.softclinic.model.TestCase;
import org.apache.http.Consts;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

public class AccessTokenScheduler extends TimerTask {

    @Override
    public void run() {
        System.out.println("AccessTokenScheduler started:"+new Date());
        getAccessToken();
        System.out.println("AccessTokenScheduler started:"+new Date());
    }

    public void getAccessToken(){
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(RestAssured.baseURI+APIConstants.REFRESH_TOKEN_API);
        post.setHeader("Content-Type","application/x-www-form-urlencoded");

        List<NameValuePair> form = new ArrayList<>();
        form.add(new BasicNameValuePair("refresh_token", RunTestCase.getRefreshToken()));
        form.add(new BasicNameValuePair("grant_type","refresh_token"));
        form.add(new BasicNameValuePair("client_id","a665fe6b-e7b4-45b5-95ef-23343544dcfa"));
        form.add(new BasicNameValuePair("client_secret","37a7c5bb-dd64-47c9-a6b0-0322d710fb89"));

        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(form, Consts.UTF_8);

        post.setEntity(entity);
        HttpResponse response = null;
        try {
            response = client.execute(post);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("API response code::"+response.getStatusLine().getStatusCode());
        String responseBody = null;
        try {
            responseBody = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonObject jsonObject = new JsonParser().parse(responseBody).getAsJsonObject();

        RunTestCase.setRefreshToken(jsonObject.get("Refreshtoken").toString().replace("\"",""));
    }
}
