package com.jvs.softclinic.model;

public class Entity {
    private String url;
    private String[] suffixRandomString;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String[] getSuffixRandomString() {
        return suffixRandomString;
    }

    public void setSuffixRandomString(String[] suffixRandomString) {
        this.suffixRandomString = suffixRandomString;
    }
}
