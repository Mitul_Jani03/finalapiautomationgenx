package com.jvs.softclinic.model;

import java.util.Map;

public class EntityMapping {
	
	private static Map<String,Object> mappings;

	public static Map<String, Object> getMappings() {
		return mappings;
	}

	public static void setMappings(Map<String, Object> mappings) {
		EntityMapping.mappings = mappings;
	}
	
}
