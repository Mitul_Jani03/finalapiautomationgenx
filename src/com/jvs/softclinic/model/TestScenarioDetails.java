package com.jvs.softclinic.model;

public class TestScenarioDetails {
	private String type;
	private String file;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	@Override
	public String toString() {
		return "TestCase [type=" + type + ", file=" + file + "]";
	}
}
