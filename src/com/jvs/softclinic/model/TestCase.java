package com.jvs.softclinic.model;

import com.google.gson.JsonObject;

public class TestCase {
	private JsonObject data;
	private Meta meta;
	private JsonObject response;
	

	public JsonObject getData() {
		return data;
	}
	public void setData(JsonObject data) {
		this.data = data;
	}
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public JsonObject getResponse() {
		return response;
	}
	public void setResponse(JsonObject response) {
		this.response = response;
	}
	@Override
	public String toString() {
		return "TestCase [data=" + data + ", meta=" + meta + ", response=" + response + "]";
	}
}
